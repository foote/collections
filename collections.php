
<html>
<head>

    <head>
        <title>Constructors</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" />
        <link rel="stylesheet" href="site.css" />
    </head>

</head>
<body>

    <h1>Welcome to Collections</h1>
    <ul>
        <li>
            <a href="typedarrays.php">Typed Arrays</a>
        </li>
        <li>
            <a href="endianness.php">Endianness</a>
        </li>
        <li>
            <a href="multiviews.php">Multiple Views</a>
        </li>
        <li>
            <a href="constructors.php">Constructors</a>
        </li>
        <li>
            <a href="maps.php">Maps</a>
        </li>
        <li>
            <a href="mapvalues.php">Map Values</a>
        </li>
        <li>
            <a href="mapkeys.php">Map Keys</a>
        </li>
        <li>
            <a href="weakmaps.php">Weak Maps</a>
        </li>
        <li>
            <a href="sets.php">Sets</a>
        </li>
        <li>
            <a href="setiterators.php">Set Iterators</a>
        </li>
        <li>
            <a href="WeakSets.php">Weak Sets</a>
        </li>


    </ul>

    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>