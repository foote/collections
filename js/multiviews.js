
function test() {

    var buf = new ArrayBuffer(2);

    var view8 = new Uint8Array(buf);
    var view16 = new Uint16Array(buf);

    view16[0] = 3085;
    //view8[0];						// 13
    //view8[1];						// 12

    console.log('view16[0] : ' + view16[0]);
    console.log('view8[0] : ' + view8[0]);
    console.log('view8[1] : ' + view8[1]);


    console.log('view8[0].toString(16) : ' + view8[0].toString(16));		// "d"
    console.log('view8[1].toString(16) : ' + view8[1].toString(16));		// "c"

    // swap (as if endian!)
    var tmp = view8[0];
    view8[0] = view8[1];
    view8[1] = tmp;

    console.log('view8[0].toString(16) : ' + view8[0].toString(16));		// "d"
    console.log('view8[1].toString(16) : ' + view8[1].toString(16));		// "c"

    console.log('view16[0] : ' + view16[0]);

}