
function test()
{
    console.log('Test');

    //A set is a collection of unique values (duplicates are ignored).

    //The API for a set is similar to map. The add(..) method takes the place of
    // the set(..) method (somewhat ironically), and there is no get(..) method.

    //Consider:

    var s = new Set();

    var x = { id: 1 },
        y = { id: 2 };

    s.add( x );
    s.add( y );
    s.add( x );

    s.size;							// 2

    s.delete( y );
    s.size;							// 1

    s.clear();
    s.size;							// 0

    //The Set(..) constructor form is similar to Map(..), in that it can receive
    // an iterable, like another set or simply an array of values. However, unlike how Map(..)
    // expects entries list (array of key/value arrays), Set(..) expects a values list (array
    // of values):

    var x = { id: 1 },
        y = { id: 2 };

    var s = new Set( [x,y] );


    // A set doesn't need a get(..) because you don't retrieve a value from a set,
    // but rather test if it is present or not, using has(..):

    var s = new Set();

    var x = { id: 1 },
        y = { id: 2 };

    s.add( x );

    s.has( x );						// true
    s.has( y );						// f

}
