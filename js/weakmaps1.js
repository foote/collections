
function test()
{
    console.log('Test');

    var m = new WeakMap();

    var x = { id: 1 },
        y = { id: 2 };

    m.set( x, "foo" );

    m.has( x );						// true
    m.has( y );						// false

    //It's important to note that a WeakMap only holds its keys weakly, not its values. Consider:

    var m = new WeakMap();

    var x = { id: 1 },
        y = { id: 2 },
        z = { id: 3 },
        w = { id: 4 };

    m.set( x, y );

    x = null;						// { id: 1 } is GC-eligible
    y = null;						// { id: 2 } is GC-eligible
    // only because { id: 1 } is

    m.set( z, w );

    w = null;						// { id: 4 } is not GC-eligible

}
