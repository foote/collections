
function test()
{
    console.log('Test');

    var m = new Map();

    var x = { id: 1 },
        y = { id: 2 };

    m.set( x, "foo" );
    m.set( y, "bar" );

    var vals = [ ...m.values() ];

    vals;							// ["foo","bar"]
    Array.from( m.values() );		// ["foo","bar"]

    $("#info").append('<li> Endian : '+ vals +'</li>');

    //As discussed in the previous section, you can iterate over a map's entries using entries()
    // (or the default map iterator). Consider:

    var m = new Map();

    var x = { id: 1 },
        y = { id: 2 };

    m.set( x, "foo" );
    m.set( y, "bar" );

    var vals = [ ...m.entries() ];

    vals[0][0] === x;				// true
    vals[0][1];						// "foo"

    vals[1][0] === y;				// true
    vals[1][1];

}
