
function test()
{
    console.log('Test');

    //Sets have the same iterator methods as maps. Their behavior is different for sets,
    // but symmetric with the behavior of map iterators. Consider:

    var s = new Set();

    var x = { id: 1 },
        y = { id: 2 };

    s.add( x ).add( y );

    var keys = [ ...s.keys() ],
        vals = [ ...s.values() ],
        entries = [ ...s.entries() ];

    keys[0] === x;
    keys[1] === y;

    vals[0] === x;
    vals[1] === y;

    entries[0][0] === x;
    entries[0][1] === x;
    entries[1][0] === y;
    entries[1][1] === y;


    //The keys() and values() iterators both yield a list of the unique values
    // in the set. The entries() iterator yields a list of entry arrays,
    // where both items of the array are the unique set value. The default
    // iterator for a set is its values() iterator.

    //The inherent uniqueness of a set is its most useful trait. For example:

    var s = new Set( [1,2,3,4,"1",2,4,"5"] ),
        uniques = [ ...s ];

    uniques;						// [1,2,3,4,"1","5"]

}
