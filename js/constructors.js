
function test()
{
    console.log('Test');

    var a = new Int32Array( 3 );
    a[0] = 10;
    a[1] = 20;
    a[2] = 30;

    a.map( function(v){
        console.log( v );
    } );  // 10 20 30

    a.join( "-" ); // "10-20-30"

    $("#info").append('<li> Endian : '+ a +'</li>');

    ///

    var a = new Uint8Array( 3 );
    a[0] = 10;
    a[1] = 20;
    a[2] = 30;

    var b = a.map( function(v){
        return v * v;
    } );

    b;				// [100, 144, 132]
    ///

    // The 20 and 30 values, when squared, resulted in bit overflow.
    // To get around such a limitation, you can use the TypedArray#from(..) function:

    var a = new Uint8Array( 3 );
    a[0] = 10;
    a[1] = 20;
    a[2] = 30;

    var b = Uint16Array.from( a, function(v){
        return v * v;
    } );

    b;				// [100, 400, 900]

    //////////////////////////////
    var a = [ 10, 1, 2, ];
    a.sort();								// [1,10,2]

    var b = new Uint8Array( [ 10, 1, 2 ] );
    b.sort();								// [1,2,10]

}
