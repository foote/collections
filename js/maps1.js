
function test()
{
    console.log('Test');


    var m = {};

    var x = { id: 1 },
        y = { id: 2 };

    m[x] = "foo";
    m[y] = "bar";

    m[x];							// "bar"
    m[y];							// "bar"

    $("#info").append('<li> m[x] : '+ m[x] +'</li>');
    $("#info").append('<li> m[y] : '+ m[y] +'</li>');

    // Some have implemented fake maps by maintaining a parallel array
    // of non-string keys alongside an array of the values, such as:

    var keys = [], vals = [];

    var x = { id: 1 },
        y = { id: 2 };

    keys.push( x );
    vals.push( "foo" );

    keys.push( y );
    vals.push( "bar" );

    keys[0] === x;					// true
    vals[0];						// "foo"

    keys[1] === y;					// true
    vals[1];						// "bar"

    //But as of ES6, there's no longer any need to do this! Just use Map(..):

     var m = new Map();

     var x = { id: 1 },
     	y = { id: 2 };

     m.set( x, "foo" );
     m.set( y, "bar" );

     m.get( x );						// "foo"
     m.get( y );						// "bar"

    // The only drawback is that you can't use the [ ] bracket ' + '
    // access syntax for setting and retrieving values. But get(..) and set(..) ' +
    // 'work perfectly suitably instead.

    //To delete an element from a map, don't use the delete operator,
    // but instead use the delete(..) method:

    m.set( x, "foo" );
    m.set( y, "bar" );

    m.delete( y );

    //You can clear the entire map's contents with clear().
    // To get the length of a map (i.e., the number of keys),
    // use the size property (not length):

    m.set( x, "foo" );
    m.set( y, "bar" );
    m.size;							// 2

    m.clear();
    m.size;							// 0

    // Object to object mapping
    var m2 = new Map( m.entries() );

    // same as:
    var m2 = new Map( m );

    // Manual Mapping - Lame
    var x = { id: 1 },
        y = { id: 2 };

    var m = new Map( [
        [ x, "foo" ],
        [ y, "bar" ]
    ] );

    m.get( x );						// "foo"
    m.get( y );						// "bar"

}
